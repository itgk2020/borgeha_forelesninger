# Det som skjer her er at vi skal oppdatere en dict med verdier.
# Som dere husker vil en krasje hvis man kjører dict[x].append()
# hvis x inne finnes allerede. Dette er en alternativ måte å
# utnytte unntak.
# Legg merke til at det er to unntak etter hverandre. 
def leggTilNoe(dikt):
    try:
        navn=input('For hvem vil du oppdatere dictionarien: ')
        leggTil = input('Oppgi verdien som skal legges til: ')
        dikt[navn].append(leggTil)
# Hver verdi er først tall. Dermed vil append krasje.
# da vil den utløse AttributeError. Her vil verdien
# gjøres om til en liste med det som fantes pluss det nye.
    except AttributeError as e:
        print('AttributeError', e)
        dikt[navn] = [dikt[navn],leggTil]
# Hvis nøkkelen ikke allerede finnes kommer vi hit:
    except KeyError as e:
        print('KeyError', e)
        dikt[navn] = leggTil

minDikt = {'Per':92925492}      # Opprette
print(minDikt)

minDikt['Nils'] = 92939495      # Legge til
print(minDikt)

leggTilNoe(minDikt)

print(minDikt)