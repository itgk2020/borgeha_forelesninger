try:
    filnavn = input('Oppgi filnavn: ')
    if '.txt' not in filnavn:
        filnavn += '.txt'
    f = open(filnavn,'r')
    innhold = f.read()
    f.close()
    print(innhold)
except IOError:
    print(f'Feil ved lesing av filen "{filnavn}"')
else:
    print(filnavn)