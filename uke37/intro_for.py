#coding=utf-8
# For - loopies

# Den enkleste - fra 0 til og uten et heltall

# Den litt mer avanserte: fra et tall til og uten et annet

# Enda mer avansert: Fra et tall, til og uten et annet tall, men hopp n hver gang!

# Avansertest: Kan man loope tegnene i en streng?

# Avansertestestere: Kan man... uhm...
# Hvis jeg vil loope gjennom mange ulike tall jeg vet på forhånd...
# Er det noe i senere pensum som kan hjelpe meg med det? Pliiiis!!!1!

# Avansertestest: kan vi... uhm... gå nedover? Telle baklengs? fra bakerste bokstav?

# Avansertestestere: Vi får se om vi har tid til en oppgave eller to om for-løkker.
# Som:
    # spør om og lagre en streng
    # spør hvor stort hopp du skal gjøre mellom hvert tegn
    # for hvert hopp gjennom strengen skriver du ut om bokstaven er en vokal:
    # bokstav in 'aeiouyæøå'
