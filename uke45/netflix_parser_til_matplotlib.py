import pickle
from matplotlib import pyplot as plt

liste = []

def read_netflix():    
    dict = {}
    with open("netflix_sane.csv", 'r', encoding="utf-8") as f:
        for line in f.readlines():
            try:
                liste.append(line.split("#"))
                item = line.split("#")
                dict[item[1]] = [item[3]] # title
                dict[item[1]].append(item[2])  # type
                dict[item[1]].append(item[4]) # country
                dict[item[1]].append(item[6]) # year
                dict[item[1]].append(item[-1]) # desc
            except IndexError as e:
                print('IndexError:', e, ":", item)
                continue
    return dict
            
def find_title(dict, name):
    for k, v in dict.items():
        if name in v[0]:
            print(v[0])
            
def find_country(dict, country):
    for v in dict.values():
        try:
            if country in v[2]:
                print(f'{country}: {v[0]} ({v[3]}).')
        except Exception as e:
            print("Exception:", e, ":", v)
            break

def save_dict(dict):
    f = open('netflix.dat', 'wb')
    pickle.dump(dict, f)
    f.close()
    
def load_dict():
    with open('netflix.dat', 'rb') as f:
        return pickle.load(f)


# Finne alle filmer per år for landet inn:
def items_per_year(dict, country):
    per_year = {}
    try:
        for v in dict.values():
            if country in v[2]:
                already = per_year.get(v[3], 0)
                per_year[v[3]] = already + 1
    except:
        print('error')
    finally:
        return per_year
    
dict = read_netflix()

# hente ut alle filmene fra Frankrike per år
france = items_per_year(dict, 'United States')

# sortert liste: Det finnes mer fancy måter å gjøre dette
years = []
items = []
for year in sorted(france):
    years.append(year)
    items.append(france[year])


# plotting
plt.plot(years, items)


# Forskjellig utseende på plotting
#plt.style.use('fivethirtyeight')
plt.style.use('ggplot')

# Du kan legge på navn på ting:
plt.xlabel('År')
plt.ylabel('Antall innslag')
plt.title('Antall innslag per år.')

plt.show()

