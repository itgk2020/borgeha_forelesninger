# Oppgave 3.1
def calculate_percentage(votes):
    dem = int(votes[0])*100/(int(votes[0]) + int(votes[1]))
    rep = 100 - dem
    return round(dem,2),round(rep,2)


# Oppgave 3.2
def return_total_votes(state_votes):
    dem, rep = 0,0
    for i in state_votes:
        dem += int(i[0])
        rep += int(i[1])
    return (dem,rep)


# Oppgave 3.3
def update_state(dict, state, demvotes, repvotes):
    if dict.get(state,0) != 0:
        dict[state].append((demvotes,repvotes))
    else:
        dict[state] = [(demvotes, repvotes)]
    return dict


# Oppgave 3.4
def read_from_file():
    dikt = {}
    with open('votes.txt','r') as f:
        linje = f.readline()
        while(linje):
            liste = linje.split(',')
            try:
                dikt = update_state(dikt, liste[0], int(liste[1]), int(liste[2]))
            except:
                print(f'Linje kunne ikke tolkes: {linje}')
            linje = f.readline().strip()
    return dikt


# Oppgave 3.5
def get_ev_for_state(state):
    electoral_votes = [["Arizona", 11], ["Nevada", 6], ["Pennsylvania", 20], ["Georgia", 16]]
    for el in electoral_votes:
        if el[0] == state:
            return el[1]


# Oppgave 3.6
def get_actual_ev(state, dempercent, reppercent):
    ev = get_ev_for_state(state)
    if dempercent > reppercent:
        return (ev,0)
    else:
        return (0,ev)
    

# Oppgave 3.7
def get_actual_ev_fair(state, dempercent, reppercent):
    ev = get_ev_for_state(state)
    dem_ev = round(dempercent*ev/100)
    return (dem_ev,ev-dem_ev)
    

# Oppgave 3.8

# Hjelpefunksjon. Dere trenger ikke å lage en slik, men viser her at
# jeg har kjørt ut en del av koden til sin egen funksjon for å forenkle
# koden i hovedoppgaven. Se at den brukes under.
def find_percent(state,votes):
    dem,rep = 0,0
    for el in votes[state]:
        dem += int(el[0])
        rep += int(el[1])
    return calculate_percentage((dem, rep))
        
    
# Her var det en veldig åpen oppgave, så det er ikke så rart at noen
# tolket det som at de skulle lese inn fil og slikt. Vi hadde i
# utangspunktet tenkt at votes inn skulle være den fra 3.4.
def print_nation_results(votes):
    dem,rep = 0,0
    ev_norm_dem,ev_norm_rep = 0,0
    ev_fair_dem,ev_fair_rep = 0,0
    for key in votes:
        stemmer = return_total_votes(votes[key])
        dem += stemmer[0]
        rep += stemmer[1]
        prosent = find_percent(key,votes)
        pro_pr_parti = get_actual_ev(key, prosent[0], prosent[1])
        ev_norm_dem += pro_pr_parti[0]
        ev_norm_rep += pro_pr_parti[1]
        pro_pr_parti = get_actual_ev_fair(key, prosent[0], prosent[1])
        ev_fair_dem += pro_pr_parti[0]
        ev_fair_rep += pro_pr_parti[1]
        
    print(f'Dems got {dem} votes, reps got {rep}')
    print(f'With wta, dems got {ev_norm_dem} while reps got {ev_norm_rep} electoral votes')
    print(f'With fair, dems got {ev_fair_dem} while reps got {ev_fair_rep} electoral votes')

    
print(f"Electoral votes for Nevada should be 6: {get_ev_for_state('Nevada')}.")
print(get_actual_ev('Nevada', 50.1,49.9))
fair = get_actual_ev_fair("Nevada", 50.1, 49.9)
print(f"Hvis vi fordeler etter prosent skal begge få 3: {fair}.")
votes = read_from_file()
print_nation_results(votes)


