import random

def car_registration():
    string = chr(random.randint(65, 65+26))
    string += chr(random.randint(65, 65+26))
    tall = random.randint(10000,99999)
    string += str(tall)
    return string
    
print(car_registration())

def prepare(tall):
    for i in tall:
        if i not in "01":
            return "Dette er ikke et binært tall"
    antall = 6-len(tall)
    return "0" * antall + tall

print(prepare("10101"))
print(prepare("1101"))

def convert(streng):
    tall = 0
    if len(streng) > 5:
        streng = streng[-5:]
    for c in streng:
        tall += ord((c))
    return chr(tall)

print(convert("abc"))
print(convert("Petter"))

def add_matrix(l1, l2):
    import numpy
    m1 = numpy.array(l1)
    m2 = numpy.array(l2)
#    print(m1, m2)
#    return m1+m2
    ll = []
    li = []
    for k in range(len(l1[0])):
        li.append(l1[0][k] + l2[0][k])
    ll.append(li)
    li = []
    for k in range(len(l1[0])):
        li.append(l1[1][k] + l2[1][k])
    ll.append(li)
    return ll
        

print(add_matrix([[1,2,3,4,5],[3,4,5,6,7]],\
                 [[2,4,6,8,9],[6,8,10,12,13]]))

def list_to_disk(l, filnavn):
    with open(filnavn, 'w') as f:
        for b, t in l:
            f.write(f"'{b}';'{t}'\n")
    
    
l = [['Beatles','And I love her'],\
     ['Beatles','All my loving'],\
     ['Traffic','John Barleycorn must die'],\
     ['Cream','Sunshine of my love'],\
     ['Cream','SLWABR']]
list_to_disk(l, 'filnavn.txt')