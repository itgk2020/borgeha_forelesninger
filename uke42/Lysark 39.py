streng = 'Skriv inn noe her'

print('Bare bokstaver og tall?',streng.isalnum())
print('Bare bokstaver?',streng.isalpha())
print('Bare tall?',streng.isdigit())
print('Bare små bokstaver?',streng.islower())
print('Bare tegn som ikke vises på skjermen?',streng.isspace())
print('Gjør om til stort:',streng.upper())
print('Gjør om til smått:',streng.lower())